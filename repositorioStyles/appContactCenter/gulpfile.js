var gulp 		= require('gulp');
var	uglify 		= require('gulp-uglify');
var	concat 		= require('gulp-concat');
var	jade 		  = require('gulp-jade');
var	sass 		  = require('gulp-sass');
var	clean 		= require('gulp-clean');
var	image 		= require('gulp-image');
var imagemin 	= require('gulp-imagemin');
var pngquant 	= require('imagemin-pngquant');

// DIRECCION DE PROYECTO LOCAL
var pathOrg = 'Desktop/proyecto_banorte/Proyecto_contact_center/repositorioStyles/appContactCenter';
// DIRECCION PARA PONER CARPETA EN RAD
var radDest = 'c:/workspaceCC/CC_Banorte_BTT_StaticStyles/WebContent/css/',
    radDestCarp = 'c:/workspaceCC/CC_Banorte_BTT_StaticStyles/WebContent/';
// BORRAR CARPETA DIST Y ClON
gulp.task('borrar', function () {
	console.log("------------------------------------------------------------------------------------------------");
	console.log("                INICIANDO   TAREAS                              ");
	console.log("------------------------------------------------------------------------------------------------");
	console.log('.. BORRANDO CARPETAS ..');
	return gulp.src(['dist/',
					pathOrg]).pipe(clean({force: true}));
});

// SASS CSS SEPARADO
gulp.task('sass', ['borrar'], function () {
	console.log('.. COMPILANDO SASS ..');

	var bienvenida = gulp.src([ 'app/sass/bienvenida.scss'],
				{base:'app/sass/'})
	.pipe(concat('bienvenida.css'))
	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
	.pipe(gulp.dest('dist/contact_center/css/'));

  var contactcenter = gulp.src([ 'app/sass/contactcenter.scss'],
        {base:'app/sass/'})
  .pipe(concat('contactcenter.css'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('dist/contact_center/css/'));

  var customized = gulp.src([ 'app/sass/Customized.scss'],
        {base:'app/sass/'})
  .pipe(concat('Customized.css'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('dist/contact_center/css/'));

  var main_2 = gulp.src([ 'app/sass/main_2.scss'],
        {base:'app/sass/'})
  .pipe(concat('main_2.css'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('dist/contact_center/css/'));

  var menu_3 = gulp.src([ 'app/sass/menu_3.scss'],
        {base:'app/sass/'})
  .pipe(concat('menu_3.css'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('dist/contact_center/css/'));

  var menu = gulp.src([ 'app/sass/menu.scss'],
        {base:'app/sass/'})
  .pipe(concat('menu.css'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('dist/contact_center/css/'));

  var main = gulp.src([ 'app/sass/main.scss'],
        {base:'app/sass/'})
  .pipe(concat('main.css'))
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(gulp.dest('dist/contact_center/css/'));

return menu,bienvenida,contactcenter,customized,main_2,menu_3,main;
});



//concatena todos los css
gulp.task('concatAllCss',['sass'], function() {
	console.log('.. CONCATENANDO CSS excepto bienvenida.css ..');
  return gulp.src(['./dist/contact_center/css/*.css','!./dist/contact_center/css/bienvenida.css'])
    .pipe(concat('contac_center.css'))
    .pipe(gulp.dest('./dist/contact_center/css'));
});

//Eliminar archivos concatenados
gulp.task('cleanAllConcatenatedCss',['concatAllCss'], function () {
	console.log('.. ELIMINA CSS excepto  contac_center_style.css y bienvenida.css ..');
    return gulp.src(['./dist/contact_center/css/*.css','!./dist/contact_center/css/bienvenida.css','!./dist/contact_center/css/contac_center.css'], {read: false})
        .pipe(clean({force: true}));
});

// JS
gulp.task('js', ['cleanAllConcatenatedCss'], function() {
	console.log('.. JAVASCRIPT ..');
	var stream = gulp.src('app/scripts/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/contact_center/js/'));
	return stream;
});

// IMAGENES
gulp.task('img', ['js'], function() {
	console.log('.. COMPRIMIENDO IMAGENES ..');
	var stream = gulp.src('app/img/**')
	.pipe(image({
		pngquant: true,
		optipng: true,
		zopflipng: true,
		advpng: true,
		jpegRecompress: true,
		jpegoptim: true,
		mozjpeg: true,
		gifsicle: true,
		svgo: true
	}))
	.pipe(gulp.dest('dist/contact_center/img/'));
	return stream;
});

gulp.task('img2', ['img'], () => {
	console.log('.. COMPRIMIENDO IMAGENES 2 ..');
	var stream = gulp.src(['dist/contact_center/img/**/*.jpg',
						   'dist/contact_center/img/**/*.png',
						   'dist/contact_center/img/**/*.svg'])
	.pipe(imagemin({
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
	}))
	.pipe(gulp.dest('dist/contact_center/img/'));
	return stream;
});



// COPIANDO FUENTES
gulp.task('co', ['img'], function () {
	console.log('.. COPIANDO FUENTES ..');
	var stream = gulp.src('app/fonts/**/*')
	.pipe(gulp.dest('dist/contact_center/fonts/'));
	return stream;
});

//  COPIANDO CARPETA DIST A OTRA LOCACION - LUGAR
gulp.task('copy', ['co'], function () {
	console.log('.. COPIANDO CARPETA DIST ..');
	var css = gulp.src('dist/contact_center/css/*.css')
	.pipe(gulp.dest(radDest));
  var img = gulp.src('dist/contact_center/fonts')
	.pipe(gulp.dest(radDestCarp));
  var fuentes = gulp.src('dist/contact_center/img')
	.pipe(gulp.dest(radDestCarp));
	return css,img,fuentes;
});

//watch
gulp.task('watch',function(){
  gulp.watch(['app/sass/*.scss'],['init']);
});



// POR PASOS
gulp.task('build', ['js','img','co','copy','watch']);

// POR STREAM
gulp.task('init', ['build'], function(){pwd

	console.log("------------------------------------------------------------------------------------------------");
	console.log("                TERMINANDO    TAREAS                              ");
	console.log("------------------------------------------------------------------------------------------------");
});
